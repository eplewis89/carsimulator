﻿using Nito.AsyncEx;
using System;
using System.Threading;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Begin Program");
                Console.WriteLine("========================================");
                AsyncContext.Run(() => MainAsync(args));
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            finally
            {
                Console.WriteLine("========================================");
                Console.WriteLine("Exiting Program");

                Thread.Sleep(Timeout.Infinite);
            }
        }

        static async void MainAsync(string[] args)
        {
            Random rand = new Random();

            int CityLength = 20;
            int CityWidth = 20;

            City MyCity = new City(CityLength, CityWidth);
            Car racecar = (RaceCar)MyCity.AddRaceCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            //Car sedan = (Sedan)MyCity.AddSedanToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            while (!passenger.IsAtDestination())
            {
                Tick(racecar, passenger);
            }

            if (passenger.IsAtDestination())
            {
                passenger.GetOutOfCar();
                racecar.DropOffPassenger();
            }
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            Console.WriteLine("==================TICK==================");
            // check if we have a passenger
            if (car.Passenger != null)
            {
                // if so we need to check the position of the car
                // and compare it to the destination
                if (!PositionUtils.PositionsMatch(car.Passenger.DestPosition, car.Position))
                {
                    // move car towards the destination
                    car.MoveToPosition(passenger.DestPosition);
                }
            }
            // if we don't have a passenger we need to pick up
            // the passenger
            else
            {
                // compare passenger and car positions
                if (PositionUtils.PositionsMatch(passenger.StartPosition, car.Position))
                {
                    // pick up the passenger
                    passenger.GetInCar(car);
                }
                else
                {
                    // move car towards passenger postion
                    car.MoveToPosition(passenger.StartPosition);
                }
            }
        }
    }
}
