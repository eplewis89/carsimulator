﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveToPosition(Position position)
        {
            if (Position.posX > position.posX)
            {
                MoveLeft();
            }
            else if (Position.posX < position.posX)
            {
                MoveRight();
            }
            else if (Position.posY > position.posY)
            {
                MoveDown();
            }
            else if (Position.posY < position.posY)
            {
                MoveUp();
            }

            if (Passenger != null)
            {
                Passenger.DownloadVeyoSite();
            }
        }

        protected override void MoveUp()
        {
            if (Position.posY < City.YMax)
            {
                Position.posY++;
                WritePositionToConsole();
            }
        }

        protected override void MoveDown()
        {
            if (Position.posY > 0)
            {
                Position.posY--;
                WritePositionToConsole();
            }
        }

        protected override void MoveRight()
        {
            if (Position.posX < City.XMax)
            {
                Position.posX++;
                WritePositionToConsole();
            }
        }

        protected override void MoveLeft()
        {
            if (Position.posX > 0)
            {
                Position.posX--;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to {0}", Position.ToString()));
        }
    }
}
