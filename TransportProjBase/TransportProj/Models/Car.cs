﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public Position Position { get; set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            Position = new Position(xPos, yPos);
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to {0}", Position.ToString()));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
            Console.WriteLine("Picked up passenger at position {0} with destination {1}", passenger.StartPosition.ToString(), passenger.DestPosition.ToString());
        }

        public void DropOffPassenger()
        {
            Console.WriteLine("Dropped off passenger at destination {0}", Passenger.DestPosition.ToString());
        }

        /// <summary>
        /// Method which moved the car to a given x,y position
        /// on the grid
        /// </summary>
        /// <param name="position">Position model containing x,y coords</param>
        public abstract void MoveToPosition(Position position);

        // the movement will be obscured from callees so as to not
        // call these methods accidentally
        protected abstract void MoveUp();

        protected abstract void MoveDown();

        protected abstract void MoveRight();

        protected abstract void MoveLeft();
    }
}
