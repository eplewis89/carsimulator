﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class Position
    {
        public int posX { get; set; }
        public int posY { get; set; }

        public Position(int setX, int setY)
        {
            posX = setX;
            posY = setY;
        }

        public override string ToString()
        {
            return String.Format("x - {0} y - {1}", posX, posY);
        }
    }
}
