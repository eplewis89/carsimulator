﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace TransportProj
{
    public class Passenger
    {
        public Position StartPosition { get; set; }
        public Position DestPosition { get; set; }
        public Car Car { get; set; }
        public City City { get; private set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            StartPosition = new Position(startXPos, startYPos);
            DestPosition = new Position(destXPos, destYPos);
            City = city;
        }

        public void GetInCar(Car car)
        {
            Car = car;
            car.PickupPassenger(this);
            Console.WriteLine("Passenger got in car.");
        }

        public void GetOutOfCar()
        {
            Car = null;
            Console.WriteLine("Passenger got out of car.");
        }

        public int GetCurrentXPos()
        {
            if (Car == null)
            {
                return StartPosition.posX;
            }
            else
            {
                return Car.Position.posX;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartPosition.posY;
            }
            else
            {
                return Car.Position.posY;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestPosition.posX && GetCurrentYPos() == DestPosition.posY;
        }

        public void DownloadVeyoSite()
        {
            Task.Run(async () => { await DownloadVeyoAsync(); }).Wait();
        }

        private Task<string> DownloadVeyoAsync()
        {
            Uri veyoURL = new Uri("https://www.veyo.com/");

            using (WebClient veyoWC = new WebClient())
            {
                veyoWC.DownloadStringCompleted += new DownloadStringCompletedEventHandler(DownloadVeyoComplete);

                return veyoWC.DownloadStringTaskAsync(veyoURL);
            }
        }

        private void DownloadVeyoComplete(object sender, DownloadStringCompletedEventArgs args)
        {
            string text = args.Result;

            Console.WriteLine("Downloaded veyo website");
        }
    }
}
