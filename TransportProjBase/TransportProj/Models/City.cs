﻿
using System;

namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddSedanToCity(int xPos, int yPos)
        {
            Console.WriteLine("Adding Sedan to City");

            Car car = (Car)new Sedan(xPos, yPos, this, null);

            return car;
        }

        public Car AddRaceCarToCity(int xPos, int yPos)
        {
            Console.WriteLine("Adding RaceCar to City");

            Car car = (Car)new RaceCar(xPos, yPos, this, null);

            return car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Console.WriteLine("Adding Passenger to City");

            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }
    }
}
