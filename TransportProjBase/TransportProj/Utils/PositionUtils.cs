﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public static class PositionUtils
    {
        /// <summary>
        /// method to check whether or not positions match
        /// </summary>
        /// <param name="posA"></param>
        /// <param name="posB"></param>
        /// <returns></returns>
        public static bool PositionsMatch(Position posA, Position posB)
        {
            bool xposmatch = false;
            bool yposmatch = false;

            if(posA.posX == posB.posX)
            {
                xposmatch = true;
            }

            if (posA.posY == posB.posY)
            {
                yposmatch = true;
            }

            return (xposmatch && yposmatch);
        }
    }
}
